package com.ruoyi.web.uploadurl;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.ruoyi.common.config.Global;

/**
 * 魔改，给springboot添加虚拟tomcat路径（静态资源）
 * @author 星灵
 *
 */
@Component
public class WebMvcConfig  implements WebMvcConfigurer{
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry  registry) {
		registry.addResourceHandler(Global.getUploadUrl()).addResourceLocations("file:"+Global.getProfile());
	}
}
