package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.base.BaseEntity;
import java.util.Date;

/**
 * 相册表 photo
 * 
 * @author ruoyi
 * @date 2018-11-28
 */
public class Photo extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 相册ID */
	private Integer photoid;
	/** 相册名称 */
	private String photoname;
	/** 封面照片 */
	private String photocover;
	/** 创建人 */
	private String creatuser;
	/** 创建时间 */
	private Date creattime;
	/** 修改人 */
	private String updateuser;
	/** 修改时间 */
	private Date updatetime;
	/** 详情 */
	private String photodetail;

	public void setPhotoid(Integer photoid) 
	{
		this.photoid = photoid;
	}

	public Integer getPhotoid() 
	{
		return photoid;
	}
	public void setPhotoname(String photoname) 
	{
		this.photoname = photoname;
	}

	public String getPhotoname() 
	{
		return photoname;
	}
	public void setPhotocover(String photocover) 
	{
		this.photocover = photocover;
	}

	public String getPhotocover() 
	{
		return photocover;
	}
	public void setCreatuser(String creatuser) 
	{
		this.creatuser = creatuser;
	}

	public String getCreatuser() 
	{
		return creatuser;
	}
	public void setCreattime(Date creattime) 
	{
		this.creattime = creattime;
	}

	public Date getCreattime() 
	{
		return creattime;
	}
	public void setUpdateuser(String updateuser) 
	{
		this.updateuser = updateuser;
	}

	public String getUpdateuser() 
	{
		return updateuser;
	}
	public void setUpdatetime(Date updatetime) 
	{
		this.updatetime = updatetime;
	}

	public Date getUpdatetime() 
	{
		return updatetime;
	}
	public void setPhotodetail(String photodetail) 
	{
		this.photodetail = photodetail;
	}

	public String getPhotodetail() 
	{
		return photodetail;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("photoid", getPhotoid())
            .append("photoname", getPhotoname())
            .append("photocover", getPhotocover())
            .append("creatuser", getCreatuser())
            .append("creattime", getCreattime())
            .append("updateuser", getUpdateuser())
            .append("updatetime", getUpdatetime())
            .append("photodetail", getPhotodetail())
            .toString();
    }
}
