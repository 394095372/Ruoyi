package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.base.BaseEntity;
import java.util.Date;

/**
 * 博客表 blog
 * 
 * @author ruoyi
 * @date 2018-12-01
 */
public class Blog extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 博文编号 */
	private Integer blogid;
	/** 博文名称 */
	private String blogname;
	/** 标签 */
	private String blogtype;
	/** 摘要 */
	private String blogabstract;
	/** 详情 */
	private String blogdetail;
	/** 创建人 */
	private String creatuser;
	/** 创建时间 */
	private Date creattime;

	public void setBlogid(Integer blogid) 
	{
		this.blogid = blogid;
	}

	public Integer getBlogid() 
	{
		return blogid;
	}
	public void setBlogname(String blogname) 
	{
		this.blogname = blogname;
	}

	public String getBlogname() 
	{
		return blogname;
	}
	public void setBlogtype(String blogtype) 
	{
		this.blogtype = blogtype;
	}

	public String getBlogtype() 
	{
		return blogtype;
	}
	public void setBlogabstract(String blogabstract) 
	{
		this.blogabstract = blogabstract;
	}

	public String getBlogabstract() 
	{
		return blogabstract;
	}
	public void setBlogdetail(String blogdetail) 
	{
		this.blogdetail = blogdetail;
	}

	public String getBlogdetail() 
	{
		return blogdetail;
	}
	public void setCreatuser(String creatuser) 
	{
		this.creatuser = creatuser;
	}

	public String getCreatuser() 
	{
		return creatuser;
	}
	public void setCreattime(Date creattime) 
	{
		this.creattime = creattime;
	}

	public Date getCreattime() 
	{
		return creattime;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("blogid", getBlogid())
            .append("blogname", getBlogname())
            .append("blogtype", getBlogtype())
            .append("blogabstract", getBlogabstract())
            .append("blogdetail", getBlogdetail())
            .append("creatuser", getCreatuser())
            .append("creattime", getCreattime())
            .toString();
    }
}
