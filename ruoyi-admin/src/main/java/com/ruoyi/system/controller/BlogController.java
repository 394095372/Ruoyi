package com.ruoyi.system.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.json.JSON;
import com.ruoyi.system.domain.Blog;
import com.ruoyi.system.service.IBlogService;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.common.base.AjaxResult;
import com.ruoyi.common.utils.ExcelUtil;

/**
 * 博客 信息操作处理
 * 
 * @author ruoyi
 * @date 2018-12-01
 */
@Controller
@RequestMapping("/system/blog")
public class BlogController extends BaseController
{
    private String prefix = "system/blog";
	
	@Autowired
	private IBlogService blogService;
	
	@RequiresPermissions("system:blog:view")
	@GetMapping()
	public String blog()
	{
	    return prefix + "/blog";
	}
	
	/**
	 * 查询博客列表
	 */
	@RequiresPermissions("system:blog:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Blog blog)
	{
		startPage();
        List<Blog> list = blogService.selectBlogList(blog);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出博客列表
	 */
	@RequiresPermissions("system:blog:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Blog blog)
    {
    	List<Blog> list = blogService.selectBlogList(blog);
        ExcelUtil<Blog> util = new ExcelUtil<Blog>(Blog.class);
        return util.exportExcel(list, "blog");
    }
	
	/**
	 * 新增博客
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存博客
	 */
	@RequiresPermissions("system:blog:add")
	@Log(title = "博客", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(Blog blog)
	{		
		return toAjax(blogService.insertBlog(blog));
	}

	/**
	 * 修改博客
	 */
	@GetMapping("/edit/{blogid}")
	public String edit(@PathVariable("blogid") Integer blogid, ModelMap mmap)
	{
		Blog blog = blogService.selectBlogById(blogid);
		mmap.put("blog", blog);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存博客
	 */
	@RequiresPermissions("system:blog:edit")
	@Log(title = "博客", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(Blog blog)
	{		
		return toAjax(blogService.updateBlog(blog));
	}
	
	/**
	 * 删除博客
	 */
	@RequiresPermissions("system:blog:remove")
	@Log(title = "博客", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(blogService.deleteBlogByIds(ids));
	}
	
	/**
	 * 写博客
	 * @return
	 */
	@GetMapping("/insert")
	@RequiresPermissions("system:blog:insert")
	public String insertBlog() {
		
		return prefix + "/insert";
	}
	/**
	 * 看博客
	 * @return
	 */
	@GetMapping("/view")
	public String viewBlog() {
		
		return prefix + "/view";
	}
	
	@PostMapping("/searchlist")
	@ResponseBody
	public List<Blog> searchlist(Blog blog) throws Exception
	{
        List<Blog> list = blogService.selectBlogList(blog);
		return list;
	} 
	
	/**
	 * 看博客
	 * @return
	 */
	@GetMapping("/viewdetail/{blogid}")
	public String viewdetailBlog(@PathVariable("blogid") Integer blogid, ModelMap mmap) {
		Blog blog = blogService.selectBlogById(blogid);
		mmap.put("blog", blog);
		return prefix + "/viewdetail";
	}
	
	/**
	 * 查看简历
	 * @return
	 */
	@GetMapping("/jianli")
	public String viewJL() {
		return prefix + "/jianli";
	}
}
