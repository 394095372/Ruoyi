package com.ruoyi.system.controller;

import java.util.Date;
import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Photo;
import com.ruoyi.system.service.IPhotoService;
import com.ruoyi.framework.util.FileUploadUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.framework.web.base.BaseController;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.common.base.AjaxResult;
import com.ruoyi.common.config.Global;
import com.ruoyi.common.utils.ExcelUtil;

/**
 * 相册 信息操作处理
 * 
 * @author ruoyi
 * @date 2018-11-28
 */
@Controller
@RequestMapping("/system/photo")
public class PhotoController extends BaseController
{
    private String prefix = "system/photo";
	
	@Autowired
	private IPhotoService photoService;
	
	@RequiresPermissions("system:photo:view")
	@GetMapping()
	public String photo()
	{
	    return prefix + "/photo";
	}
	
	/**
	 * 查询相册列表
	 */
	@RequiresPermissions("system:photo:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Photo photo)
	{
		startPage();
        List<Photo> list = photoService.selectPhotoList(photo);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出相册列表
	 */
	@RequiresPermissions("system:photo:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Photo photo)
    {
    	List<Photo> list = photoService.selectPhotoList(photo);
        ExcelUtil<Photo> util = new ExcelUtil<Photo>(Photo.class);
        return util.exportExcel(list, "photo");
    }
	
	/**
	 * 新增相册
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存相册
	 */
	@RequiresPermissions("system:photo:add")
	@Log(title = "相册", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(Photo photo)
	{	
		photo.setCreateTime(new Date());
		photo.setCreatuser(ShiroUtils.getLoginName());
		return toAjax(photoService.insertPhoto(photo));
	}

	/**
	 * 修改相册
	 */
	@GetMapping("/edit/{photoid}")
	public String edit(@PathVariable("photoid") Integer photoid, ModelMap mmap)
	{
		Photo photo = photoService.selectPhotoById(photoid);
		mmap.put("photo", photo);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存相册
	 */
	@RequiresPermissions("system:photo:edit")
	@Log(title = "相册", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(Photo photo)
	{		
		return toAjax(photoService.updatePhoto(photo));
	}
	
	/**
	 * 删除相册
	 */
	@RequiresPermissions("system:photo:remove")
	@Log(title = "相册", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(photoService.deletePhotoByIds(ids));
	}
	
	/**
	 * 封面照片上传
	 * @return
	 */
	@RequiresPermissions("system:photo:upload")
	@Log(title = "相册", businessType = BusinessType.IMPORT)
	@PostMapping( "/upload")
	@ResponseBody
	public AjaxResult upload(@RequestParam("photocoverup") MultipartFile file) {

        try
        {
            if (!file.isEmpty())
            {
                String filename = FileUploadUtils.upload(Global.getProfile(), file);
                return success().put("name", filename);
            }else {
            	return error();
            }
            
        }
        catch (Exception e)
        {
            return error(e.getMessage());
        }
    
	}
	
	@GetMapping("/detail/{photoid}")
	public String detail(@PathVariable("photoid") Integer photoid, ModelMap mmap) {
		Photo photo = photoService.selectPhotoById(photoid);
		mmap.put("photo", photo);
		 return prefix + "/detail";
	}
	
	/**
	 * 封面照片上传
	 * @return
	 */
	@RequiresPermissions("system:photo:upload")
	@Log(title = "相册", businessType = BusinessType.IMPORT)
	@PostMapping( "/uploaddetail")
	@ResponseBody
	public AjaxResult uploaddetail(@RequestParam("photodetailcoverup") MultipartFile[] photodetailcoverup) {

        try
        {
            if (null!=photodetailcoverup&&photodetailcoverup.length!=0)
            {   
            	String name="";
            	for(MultipartFile file:photodetailcoverup) {
                    String filename = FileUploadUtils.upload(Global.getProfile(), file);
                    name+=filename+",";
            	}
            	name=name.substring(0,name.length()-1);
            	System.out.println(name);
                return success().put("name", name);
            }else {
            	return error();
            }
            
        }
        catch (Exception e)
        {
            return error(e.getMessage());
        }
    
	}
	
	/**
	 * 相册预览
	 * @return
	 */
	@GetMapping("/view")
	public String view() {
		return prefix + "/view";
	}
	
	@GetMapping("/viewdetail/{photoid}")
	public String viewdetail(@PathVariable("photoid") Integer photoid, ModelMap mmap) {
		Photo photo = photoService.selectPhotoById(photoid);
		mmap.put("photo", photo);
		return prefix + "/viewdetail";
	}
}
