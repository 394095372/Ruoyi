package com.ruoyi.system.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.druid.support.json.JSONParser;
import com.alibaba.druid.support.json.JSONUtils;
import com.ruoyi.common.config.Global;
import com.ruoyi.common.json.JSON;
import com.ruoyi.framework.util.FileUploadUtils;

/**
 * ck上传图片
 * @author 星灵
 *
 */
@Controller
@RequestMapping("/ckupload")
public class CkeditorUploadController {
	  /*
     * 图片命名格式
     */
   // private static final String DEFAULT_SUB_FOLDER_FORMAT_AUTO = "yyyyMMddHHmmss";
    
    @RequestMapping(value = "uploadImg")
    public void uplodaImg(@RequestParam("upload") MultipartFile file,//
            HttpServletRequest request, //
            HttpServletResponse response)//
    {
    	try {
    	if (!file.isEmpty()) {
    		String filename = FileUploadUtils.upload(Global.getProfile()+"blog/", file);    		
			PrintWriter out = response.getWriter();
            //String CKEditorFuncNum = request.getParameter("CKEditorFuncNum");
            Map a=new HashMap<>();
            a.put("uploaded", 1);
            a.put("fileName",filename );
            a.put("url", "/uploads"
            		+ "/blog/"+filename);
            out.println(JSON.marshal(a));
			} 
    	}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
