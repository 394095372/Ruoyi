package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.Photo;
import java.util.List;	

/**
 * 相册 数据层
 * 
 * @author ruoyi
 * @date 2018-11-28
 */
public interface PhotoMapper 
{
	/**
     * 查询相册信息
     * 
     * @param photoid 相册ID
     * @return 相册信息
     */
	public Photo selectPhotoById(Integer photoid);
	
	/**
     * 查询相册列表
     * 
     * @param photo 相册信息
     * @return 相册集合
     */
	public List<Photo> selectPhotoList(Photo photo);
	
	/**
     * 新增相册
     * 
     * @param photo 相册信息
     * @return 结果
     */
	public int insertPhoto(Photo photo);
	
	/**
     * 修改相册
     * 
     * @param photo 相册信息
     * @return 结果
     */
	public int updatePhoto(Photo photo);
	
	/**
     * 删除相册
     * 
     * @param photoid 相册ID
     * @return 结果
     */
	public int deletePhotoById(Integer photoid);
	
	/**
     * 批量删除相册
     * 
     * @param photoids 需要删除的数据ID
     * @return 结果
     */
	public int deletePhotoByIds(String[] photoids);
	
}