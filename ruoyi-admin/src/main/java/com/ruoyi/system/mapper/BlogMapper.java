package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.Blog;
import java.util.List;	

/**
 * 博客 数据层
 * 
 * @author ruoyi
 * @date 2018-12-01
 */
public interface BlogMapper 
{
	/**
     * 查询博客信息
     * 
     * @param blogid 博客ID
     * @return 博客信息
     */
	public Blog selectBlogById(Integer blogid);
	
	/**
     * 查询博客列表
     * 
     * @param blog 博客信息
     * @return 博客集合
     */
	public List<Blog> selectBlogList(Blog blog);
	
	/**
     * 新增博客
     * 
     * @param blog 博客信息
     * @return 结果
     */
	public int insertBlog(Blog blog);
	
	/**
     * 修改博客
     * 
     * @param blog 博客信息
     * @return 结果
     */
	public int updateBlog(Blog blog);
	
	/**
     * 删除博客
     * 
     * @param blogid 博客ID
     * @return 结果
     */
	public int deleteBlogById(Integer blogid);
	
	/**
     * 批量删除博客
     * 
     * @param blogids 需要删除的数据ID
     * @return 结果
     */
	public int deleteBlogByIds(String[] blogids);
	
}