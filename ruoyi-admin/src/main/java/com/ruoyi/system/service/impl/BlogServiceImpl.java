package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BlogMapper;
import com.ruoyi.system.domain.Blog;
import com.ruoyi.system.service.IBlogService;
import com.ruoyi.common.support.Convert;

/**
 * 博客 服务层实现
 * 
 * @author ruoyi
 * @date 2018-12-01
 */
@Service
public class BlogServiceImpl implements IBlogService 
{
	@Autowired
	private BlogMapper blogMapper;

	/**
     * 查询博客信息
     * 
     * @param blogid 博客ID
     * @return 博客信息
     */
    @Override
	public Blog selectBlogById(Integer blogid)
	{
	    return blogMapper.selectBlogById(blogid);
	}
	
	/**
     * 查询博客列表
     * 
     * @param blog 博客信息
     * @return 博客集合
     */
	@Override
	public List<Blog> selectBlogList(Blog blog)
	{
	    return blogMapper.selectBlogList(blog);
	}
	
    /**
     * 新增博客
     * 
     * @param blog 博客信息
     * @return 结果
     */
	@Override
	public int insertBlog(Blog blog)
	{
	    return blogMapper.insertBlog(blog);
	}
	
	/**
     * 修改博客
     * 
     * @param blog 博客信息
     * @return 结果
     */
	@Override
	public int updateBlog(Blog blog)
	{
	    return blogMapper.updateBlog(blog);
	}

	/**
     * 删除博客对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteBlogByIds(String ids)
	{
		return blogMapper.deleteBlogByIds(Convert.toStrArray(ids));
	}
	
}
