package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.PhotoMapper;
import com.ruoyi.system.domain.Photo;
import com.ruoyi.system.service.IPhotoService;
import com.ruoyi.common.support.Convert;

/**
 * 相册 服务层实现
 * 
 * @author ruoyi
 * @date 2018-11-28
 */
@Service
public class PhotoServiceImpl implements IPhotoService 
{
	@Autowired
	private PhotoMapper photoMapper;

	/**
     * 查询相册信息
     * 
     * @param photoid 相册ID
     * @return 相册信息
     */
    @Override
	public Photo selectPhotoById(Integer photoid)
	{
	    return photoMapper.selectPhotoById(photoid);
	}
	
	/**
     * 查询相册列表
     * 
     * @param photo 相册信息
     * @return 相册集合
     */
	@Override
	public List<Photo> selectPhotoList(Photo photo)
	{
	    return photoMapper.selectPhotoList(photo);
	}
	
    /**
     * 新增相册
     * 
     * @param photo 相册信息
     * @return 结果
     */
	@Override
	public int insertPhoto(Photo photo)
	{
	    return photoMapper.insertPhoto(photo);
	}
	
	/**
     * 修改相册
     * 
     * @param photo 相册信息
     * @return 结果
     */
	@Override
	public int updatePhoto(Photo photo)
	{
	    return photoMapper.updatePhoto(photo);
	}

	/**
     * 删除相册对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deletePhotoByIds(String ids)
	{
		return photoMapper.deletePhotoByIds(Convert.toStrArray(ids));
	}
	
}
